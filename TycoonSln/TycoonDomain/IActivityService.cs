﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TycoonDomain
{
    public interface IActivityService
    {
        Task<Activity> CreateActivityAsync(ActivityCreationDto activityDto);
        Task<IEnumerable<Activity>> GetAllActivitiesAsync();
        Task<Activity> GetActivityByIdAsync(string id);
        
    }

}
