﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TycoonDomain
{
    public abstract class Activity
    {
        public string Id { get; private set; }
        public string WorkerId { get; private set; }
        public DateTime StartTime { get; private set; }
        public DateTime EndTime { get; private set; }
        public ActivityType Type { get; protected set; }

      
        public Activity(string id, string workerId, DateTime startTime, DateTime endTime, ActivityType type)
        {
            Id = id ?? throw new ArgumentNullException(nameof(id));
            WorkerId = workerId ?? throw new ArgumentNullException(nameof(workerId));
            StartTime = startTime;
            EndTime = endTime;
            Type = type;

            ValidateTimes();
        }

        private void ValidateTimes()
        {
            if (StartTime >= EndTime)
            {
                throw new ArgumentException("End time must be greater than start time.");
            }
        }

        
        public TimeSpan GetRequiredRestPeriod()
        {
            switch (Type)
            {
                case ActivityType.Component:
                    return TimeSpan.FromHours(2);
                case ActivityType.Machine:
                    return TimeSpan.FromHours(4);
                default:
                    return TimeSpan.Zero;
            }
        }

    }

}
