﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TycoonDomain
{
    public interface IWorkerRepository
    {
        Task<Worker> AddAsync(Worker worker);
        Task<Worker> GetByIdAsync(string id);
        Task<IEnumerable<Worker>> GetAllAsync();
        Task<Worker> UpdateAsync(Worker worker);
        Task<bool> DeleteAsync(string id);
    }
}
