﻿namespace TycoonDomain
{
    public class Worker
    {
        public string Id { get; private set; }
        public string Name { get; private set; } 

        public Worker(string id, string name)
        {
            Id = id;
            Name = name;
        }
    }
}
