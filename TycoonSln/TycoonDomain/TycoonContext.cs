﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace TycoonDomain
{
    public class TycoonContext : DbContext
    {
        public TycoonContext(DbContextOptions<TycoonContext> options)
            : base(options)
        {
        }

        public DbSet<Worker> Workers { get; set; }
        public DbSet<Activity> Activities { get; set; }

     

         protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ComponentActivity>().HasBaseType<Activity>();
            
            modelBuilder.Entity<Activity>().HasKey(a => a.Id);
        }

    }
}
