﻿using Microsoft.EntityFrameworkCore;
using TycoonDomain;

public class WorkerEfRepository : IWorkerRepository
{
    private readonly TycoonContext _context;

    public WorkerEfRepository(TycoonContext context)
    {
        _context = context;
    }

    public async Task<Worker> AddAsync(Worker worker)
    {
        await _context.Workers.AddAsync(worker);
        await _context.SaveChangesAsync();
        return worker;
    }

    public Task<bool> DeleteAsync(string id)
    {
        throw new NotImplementedException();
    }

    public async Task<IEnumerable<Worker>> GetAllAsync()
    {
        return await _context.Workers.ToListAsync(); 
    }

    public async Task<Worker> GetByIdAsync(string id)
    {
    
        return await _context.Workers.FindAsync(id);
    }

    public Task<Worker> UpdateAsync(Worker worker)
    {
        throw new NotImplementedException();
    }


}