﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TycoonDomain
{
    public class ActivityService : IActivityService
    {
        private readonly IActivityRepository _activityRepository;
        private readonly IWorkerRepository _workerRepository; 

        public ActivityService(IActivityRepository activityRepository, IWorkerRepository workerRepository)
        {
            _activityRepository = activityRepository;
            _workerRepository = workerRepository;
        }

        public async Task<Activity> CreateActivityAsync(ActivityCreationDto activityDto)
        {
            
            if (activityDto.StartTime >= activityDto.EndTime)
            {
                throw new ArgumentException("End time must be after start time.");
            }

            var worker = await _workerRepository.GetByIdAsync(activityDto.WorkerId);
            if (worker == null)
            {
                throw new ArgumentException("Worker not found.");
            }

            Activity activity = null;

            
            switch (activityDto.Type)
            {
                case "BuildComponent":
                    string newActivityId = Guid.NewGuid().ToString();

                    activity = new ComponentActivity(
                        newActivityId,
                        activityDto.WorkerId,
                        activityDto.StartTime,
                        activityDto.EndTime
                    );
                    break;
           
                default:
                    throw new ArgumentException("Invalid activity type.");
            }

           
            return await _activityRepository.AddAsync(activity);
        }

        public async Task<IEnumerable<Activity>> GetAllActivitiesAsync()
        {
            return await _activityRepository.GetAllAsync();
        }

        public async Task<Activity> GetActivityByIdAsync(string id)
        {
          
            return await _activityRepository.GetByIdAsync(id);
        }

        
    }
}
