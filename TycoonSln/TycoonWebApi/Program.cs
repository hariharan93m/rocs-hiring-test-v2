using Microsoft.EntityFrameworkCore;
using TycoonDomain;

var builder = WebApplication.CreateBuilder(args);


builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();


builder.Services.AddDbContext<TycoonContext>(options =>
{
    options.UseInMemoryDatabase("TycoonDb"); 
    if (builder.Environment.IsDevelopment())
    {
        options.EnableSensitiveDataLogging(); 
    }
});

builder.Services.AddScoped<IWorkerRepository, WorkerEfRepository>();
builder.Services.AddScoped<IActivityRepository, ActivityEfRepository>();
builder.Services.AddScoped<IWorkerService, WorkerService>();
builder.Services.AddScoped<IActivityService, ActivityService>();

var app = builder.Build();


if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();
app.UseAuthorization();
app.MapControllers();

app.Run();
