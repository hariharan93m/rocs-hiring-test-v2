﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TycoonDomain; 
public class ActivityEfRepository : IActivityRepository
{
    private readonly TycoonContext _context;

    public ActivityEfRepository(TycoonContext context)
    {
        _context = context;
    }

    public async Task<Activity> AddAsync(Activity activity)
    {
        await _context.Activities.AddAsync(activity);
        await _context.SaveChangesAsync();
        return activity;
    }

    public async Task<bool> DeleteAsync(string id)
    {
        var activity = await _context.Activities.FindAsync(id);
        if (activity == null) return false;

        _context.Activities.Remove(activity);
        await _context.SaveChangesAsync();
        return true;
    }

    public async Task<Activity> GetByIdAsync(string id)
    {
        return await _context.Activities.FindAsync(id);
    }

    public async Task<IEnumerable<Activity>> GetAllAsync()
    {
        return await _context.Activities.ToListAsync();
    }

   

    public async Task<Activity> UpdateAsync(Activity activityToUpdate)
    {
        var activity = await _context.Activities.FindAsync(activityToUpdate.Id);
        if (activity != null)
        {
            _context.Entry(activity).CurrentValues.SetValues(activityToUpdate);
            await _context.SaveChangesAsync();
            return activityToUpdate;
        }

        return null;
    }
}
