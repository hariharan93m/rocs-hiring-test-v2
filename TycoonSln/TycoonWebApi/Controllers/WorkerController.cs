﻿using Microsoft.AspNetCore.Mvc;
using TycoonDomain;

namespace TycoonWebApi.Controllers
{
    [ApiController]
    [Route("Worker")]
    public class WorkerController : ControllerBase
    {
        private readonly IWorkerService _workerService;

        public WorkerController(IWorkerService workerService)
        {
            _workerService = workerService;
        }

        [HttpGet("{id}", Name = "GetWorker")]
        public async Task<ActionResult<Worker>> GetWorker(string id)
        {
            var worker = await _workerService.GetWorkerByIdAsync(id);

            if (worker == null)
            {
                return NotFound();
            }

            return worker;
        }


        [HttpPost]
        public async Task<ActionResult<Worker>> CreateWorker([FromBody] WorkerDto workerDto)
        {
            var worker = new Worker(workerDto.Id, workerDto.Name);
            var createdWorker = await _workerService.CreateWorkerAsync(worker);

            if (createdWorker == null)
            {
                return BadRequest();
            }

            var url = Url.Link("GetWorker", new { id = createdWorker.Id });
            if (url == null)
            {
                return BadRequest("Could not generate URL for created worker.");
            }

            var locationUri = new Uri(url);
            return Created(locationUri, createdWorker);
        }


        [HttpGet("/Workers")]
        public async Task<ActionResult<IEnumerable<Worker>>> GetAllWorkers()
        {
            var workers = await _workerService.GetAllWorkersAsync();
            return Ok(workers);  
        }


    }
}
