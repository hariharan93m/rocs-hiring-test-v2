﻿using TycoonDomain;

public class MachineActivity : Activity
{

    public MachineActivity(string id, string workerId, DateTime startTime, DateTime endTime)
        : base(id, workerId, startTime, endTime, ActivityType.Machine) 
    {
      
    }
}
