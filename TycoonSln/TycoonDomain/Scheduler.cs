﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TycoonDomain
{
    public class Scheduler
    {
        private readonly List<Activity> _scheduledActivities = new();

        public void ScheduleActivity(Activity activity)
        {
            if (_scheduledActivities.Any(a => a.EndTime.AddHours(2) > activity.StartTime)) 
            {
                throw new InvalidOperationException("Insufficient rest period between activities.");
            }

            _scheduledActivities.Add(activity);
        }

       
    }
}
