﻿using System.Collections.Generic;
using System.Linq;

namespace TycoonDomain
{
    public class WorkerRepository : IWorkerRepository
    {
        private readonly List<Worker> _workers = new();

        private static readonly Dictionary<string, Worker> Workers = new Dictionary<string, Worker>();

        public async Task<Worker> AddAsync(Worker worker)
        {
           
            await Task.Delay(1);

         
            if (Workers.ContainsKey(worker.Id))
            {
                throw new InvalidOperationException($"A worker with the ID '{worker.Id}' already exists.");
            }

            
            Workers.Add(worker.Id, worker);

            
            return worker;
        }

        public void Add(Worker worker)
        {
            _workers.Add(worker);
        }

        

        public Task<bool> DeleteAsync(string id)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<Worker>> GetAllAsync()
        {
            throw new NotImplementedException();
        }

        public Task<Worker> GetByIdAsync(string id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Worker> ListAll()
        {
            return _workers.AsReadOnly();
        }

        public Task<Worker> UpdateAsync(Worker worker)
        {
            throw new NotImplementedException();
        }

        
    }
}
