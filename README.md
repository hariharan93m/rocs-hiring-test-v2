This project is designed to manage a factory's operations using automated android workers. It aims to schedule and oversee various activities, with a focus on optimizing worker allocation and ensuring efficient operation.

Current Status
Important Update for Reviewers: The project is in an active development phase. Currently, the implemented and functional features are centered around worker management. These include:

Worker Creation: Ability to add new workers to the system.
Worker Retrieval: Functionality to list and retrieve details of existing workers.
Activity Management
The aspects related to activity management, including the scheduling of 'Build Component' or 'Build Machine' tasks and the associated logic (e.g., rest periods, team assignments), are under development and not yet functional in the current build.
