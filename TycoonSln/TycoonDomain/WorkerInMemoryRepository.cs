﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TycoonDomain; 

public class WorkerInMemoryRepository : IWorkerRepository
{
    private readonly List<Worker> _workers = new();

    public Task<Worker> AddAsync(Worker worker)
    {
        _workers.Add(worker);
        return Task.FromResult(worker);
    }

    public Task<bool> DeleteAsync(string id)
    {
        var worker = _workers.FirstOrDefault(w => w.Id == id);
        if (worker == null) return Task.FromResult(false);

        _workers.Remove(worker);
        return Task.FromResult(true);
    }

    public Task<IEnumerable<Worker>> GetAllAsync()
    {
        return Task.FromResult(_workers.AsEnumerable());
    }

    public Task<Worker> GetByIdAsync(string id)
    {
        var worker = _workers.FirstOrDefault(w => w.Id == id);
        return Task.FromResult(worker);
    }

    public Task<Worker> UpdateAsync(Worker workerToUpdate)
    {
        var worker = _workers.FirstOrDefault(w => w.Id == workerToUpdate.Id);
        if (worker != null)
        {
            _workers.Remove(worker);
            _workers.Add(workerToUpdate);
            return Task.FromResult(workerToUpdate);
        }

        return Task.FromResult<Worker>(null);
    }
}
