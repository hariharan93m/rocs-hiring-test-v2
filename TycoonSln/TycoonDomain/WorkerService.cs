﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TycoonDomain
{
    public class WorkerService : IWorkerService
    {
        private readonly IWorkerRepository _workerRepository;

        public WorkerService(IWorkerRepository workerRepository)
        {
            _workerRepository = workerRepository;
        }

        public async Task<Worker> CreateWorkerAsync(Worker worker)
        {
       
            return await _workerRepository.AddAsync(worker);
        }

        public async Task<IEnumerable<Worker>> GetAllWorkersAsync()
        {
            return await _workerRepository.GetAllAsync();
        }

        public async Task<Worker> GetWorkerByIdAsync(string id)
        {
            return await _workerRepository.GetByIdAsync(id);
        }


      
    }
}
