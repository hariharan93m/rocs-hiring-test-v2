﻿using Microsoft.AspNetCore.Mvc;
using TycoonDomain;

namespace TycoonWebApi.Controllers
{
    [ApiController]
    [Route("api/activities")]
    public class ActivitiesController : ControllerBase
    {
        private readonly IActivityService _activityService;

        public ActivitiesController(IActivityService activityService)
        {
            _activityService = activityService;
        }

        [HttpPost]
        public async Task<ActionResult<Activity>> CreateActivity([FromBody] ActivityCreationDto activityDto)
        {
            var activity = await _activityService.CreateActivityAsync(activityDto);

            if (activity == null)
            {
                return BadRequest();
            }

            return CreatedAtAction(nameof(GetActivity), new { id = activity.Id }, activity);
        }

        
        [HttpGet("{id}", Name = "GetActivity")]
        public async Task<ActionResult<Activity>> GetActivity(string id)
        {
            var activity = await _activityService.GetActivityByIdAsync(id);

            if (activity == null)
            {
                return NotFound();
            }

            return Ok(activity);
        }
    }

    


}
