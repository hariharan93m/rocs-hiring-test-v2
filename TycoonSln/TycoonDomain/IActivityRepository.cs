﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TycoonDomain
{
    public interface IActivityRepository
    {
        Task<Activity> AddAsync(Activity activity);
        Task<Activity> GetByIdAsync(string id);
        Task<IEnumerable<Activity>> GetAllAsync();
        Task<Activity> UpdateAsync(Activity activity);
        Task<bool> DeleteAsync(string id);
    }
}
