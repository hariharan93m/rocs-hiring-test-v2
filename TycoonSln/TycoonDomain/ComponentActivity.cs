﻿using System;

namespace TycoonDomain
{
    public class ComponentActivity : Activity
    {
        public ComponentActivity(string id, string workerId, DateTime startTime, DateTime endTime)
            : base(id, workerId, startTime, endTime, ActivityType.Component) 
        {
           
        }

       
    }
}
